using Godot;
using System;

public partial class TileMap : Godot.TileMap
{
   int map_size = 1000;
   int loops = 100;
   double loop_duration = 0;
   double duration = 0;
   int tile_id = 0;
   FastNoiseLite fast_noise = new FastNoiseLite();
   float noise_value;
   double start_time;
   double finish_time;
   // Called when the node enters the scene tree for the first time.
   public override void _Ready()
   {
      for (int i = 0; i < loops; i++)
      {
         fill_tilemap();
         duration = duration + loop_duration;
      }
      GD.Print("==================");
      GD.Print(duration / loops);
   }
   public void fill_tilemap()
   {
      start_time = Time.GetUnixTimeFromSystem();
      Random rd = new Random();
      int rand_num = rd.Next(0,1000);

      fast_noise.Seed = rand_num;
      for (int x = 0; x < map_size; x++)
      {
         for (int y = 0; y < map_size; y++)
         {
            var coords = new Vector2I(x ,y);
            noise_value = fast_noise.GetNoise2D(x,y);
            if (noise_value < 0)
            {
               tile_id = 0;
            }
            else
            {  
               tile_id = 1;
            }
            SetCell(0,coords,tile_id,Vector2I.Zero);
         }
         
      }
      finish_time = Time.GetUnixTimeFromSystem();
      loop_duration = finish_time - start_time;
      GD.Print(loop_duration);
   }

}

